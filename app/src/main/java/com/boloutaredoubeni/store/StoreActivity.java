package com.boloutaredoubeni.store;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class StoreActivity extends AppCompatActivity {

  private EditText mUIKeyEdit, mUIValueEdit;
  private Spinner mUITypeSpinner;
  private Button mUIGetButton, mUISetButton;
  private Store mStore;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_store);
    mStore = new Store();
  }


  private void onGetValue() {
    String lKey = mUIKeyEdit.getText().toString();
    StoreType lType = (StoreType)mUITypeSpinner.getSelectedItem();

    switch (lType) {
      case Integer:
        mUIValueEdit.setText(Integer.getInteger(lKey));
        break;
      case String:
        mUIValueEdit.setText(mStore.getString(lKey));
        break;
    }
  }

  private void onSetValue() {
    String lKey = mUIKeyEdit.getText().toString();
    String lValue = mUIValueEdit.getText().toString();
    StoreType lType = (StoreType)mUITypeSpinner.getSelectedItem();

    try {
      switch(lType) {
        case Integer:
          mStore.setInteger(lKey, Integer.parseInt(lValue));
          break;
        case String:
          mStore.setString(lKey, lValue);
          break;
      }
    } catch (NumberFormatException e) {
      displayError("Incorrect value.");
    }
  }

  private void displayError(String pError) {
    Toast.makeText(getApplicationContext(), pError, Toast.LENGTH_LONG).show();
  }
}
