package com.boloutaredoubeni.store;

/**
 * Front end to the native code
 */
public class Store {

  static {
    System.loadLibrary("ndk-store");
  }

  public native int getInteger(String pKey);
  public native void setInteger(String pKey, int pInt);

  public native String getString(String pKey);
  public native void setString(String pKey, String pString);
}
