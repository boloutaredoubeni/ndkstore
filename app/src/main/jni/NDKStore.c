//
// Created by Boloutare Doubeni on 10/17/15.
//

#include "Store.h"

#include <stdint.h>
#include <string.h>

static Store gStore = { {}, 0 };

jint
Java_com_boloutaredoubeni_store_Store_getInteger(JNIEnv* pEnv, jobject pThis, jstring pKey) {
  StoreEntry* lEntry = findEntry(pEnv, &gStore, pKey, NULL);
  if (isEntryValid(pEnv, lEntry, StoreType_Integer)) {
    return lEntry->mValue.mInteger;
  } else {
    return (jint) 0.0f;
  }
}

void
Java_com_boloutaredoubeni_store_Store_setInteger(JNIEnv* pEnv,
                                                 jobject pThis,
                                                 jstring pKey,
                                                 jint pInteger) {
  StoreEntry* lEntry = allocateEntry(pEnv, &gStore, pKey);
  if (lEntry != NULL) {
    lEntry->mType = StoreType_Integer;
    lEntry->mValue.mInteger = pInteger;
  }
}

jstring
Java_com_boloutaredoubeni_store_Store_getString(JNIEnv* pEnv, jobject pThis, jstring pKey) {
  StoreEntry* lEntry = findEntry(pEnv, &gStore, pKey, NULL);
  if (isEntryValid(pEnv, lEntry, StoreType_String)) {
    return (*pEnv)->NewStringUTF(pEnv, lEntry->mValue.mString);
  } else {
    return NULL;
  }
}

void 
Java_com_boloutaredoubeni_store_Store_setString(JNIEnv* pEnv,
                                                     jobject pThis,
                                                     jstring pKey,
                                                     jstring pString) {
  const char* lStringTmp = (*pEnv)->GetStringUTFChars(pEnv, pString, NULL);
  if (lStringTmp == NULL) {
    return;
  }

  StoreEntry* lEntry = allocateEntry(pEnv, &gStore, pKey);
  if (lEntry != NULL) {
    lEntry->mType = StoreType_String;
    jsize lStringLength = (*pEnv)->GetStringUTFLength(pEnv, pString);
    lEntry->mValue.mString = (char*) malloc(sizeof(char) * (lStringLength + 1));
    strcpy(lEntry->mValue.mString, lStringTmp);
  }
  (*pEnv)->ReleaseStringUTFChars(pEnv, pString, lStringTmp);
}
