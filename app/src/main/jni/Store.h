//
// Created by Boloutare Doubeni on 10/17/15.
//

#ifndef STORE_STORE_H
#define STORE_STORE_H

#include <stdint.h>
#include <jni.h>

#define STORE_MAX_CAPACITY 16

typedef enum {
    StoreType_Integer, StoreType_String
} StoreType;

typedef union {
    int32_t mInteger;
    char* mString;
} StoreValue;

typedef struct {
    char* mKey;
    StoreType mType;
    StoreValue mValue;
} StoreEntry;

typedef struct {
    StoreEntry mEntries[STORE_MAX_CAPACITY];
    int32_t mLength;
} Store;

/**
 * Checks if the entry allocated has the expected type
 */
int32_t isEntryValid(JNIEnv* pEnv, StoreEntry* pEntry, StoreType pType);

/**
 * Create a new entry or return an existing one
 */
StoreEntry* allocateEntry(JNIEnv* pEnv, Store* pStore, jstring pKey);

/**
 * Compares the key with every stored to find the matching one
 */
StoreEntry* findEntry(JNIEnv* pEnv, Store* pStore, jstring pKey, int32_t* pError);
void releaseEntryValue(JNIEnv* pEnv, StoreEntry* pEntry);


#endif //STORE_STORE_H
